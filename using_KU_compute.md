# KU compute


## Overview
"KU compute" is a shared Linux computing server for staff of University of Copenhagen. It is capable of relatively heavy computing tasks. This guide provides a general instructions for common computing tasks.


## Getting access
You need to request the access from the IT department of the university. Ask Lars or Jan about that.


## WinSCP
When you work you will probably need to access files on the server sometimes (for example if you write a plot to a file).
One program you can use for that is WinSCP.

* Install winscp from [here](https://winscp.net/eng/index.php)
* to connect fill in the server details:
  * host name: compute12
  * username: XXXYYY
  * password: whateveritis
* Then you can click "save" to save the details for another time
* Click "login" to connect to the server.
* You can now browse around the server. You should see a folder "groupdirs" where you can find the i-drive.


## Install Xming
To be able to plot from the server to our own computer we need a small program called xming. It acts like a receiver for the plotting in R.

* Install Xming from (here)[http://sourceforge.net/projects/xming/]
* This program should be running when you use the server.


## Install Git
The program to connect to the server (ssh) that comes with windows is a bit lacking. We will therefore use Git Bash instead.
You need to install Git and set Git Bash as the terminal for RStudio. See how [here](https://github.com/rstudio/rstudio/issues/2224#issuecomment-368260312).


## Compute through RStudio
* Click terminal (next to the console).
* Type "export DISPLAY=localhost:0" (with no citation marks). This makes the plotting work. You don't need to understand...
* Type "ssh -XY compute12". This connects you to the remote server.
* Enter you password (the same as you login in your KU computer and access your KUnet)
* Type "R" to start R
* When you run code, instead of using the shortcut "Ctrl + Enter" which sends the code to the console, use "Ctrl + Alt + Enter" which sends them to the terminal.
* I and H drive are mounted. The path is "~/groupdirs" and "~/hdrive", respectively. So adjust your scripts accordingly.


### Saving plots
The window you get when plotting does not have facilities to save the plot like in RStudio. You need to manually write out a file.

If you are using `ggplot2` you can save a plot like this:

#### ggplot2
```r
library(ggplot2)
p <- ggplot(diamonds, aes(x=carat, y = color), color="steelblue") +
      geom_point()

p

ggsave("ggtest.png", p)
```

#### Other
For any plot that is not a `ggplot` you can use `png()`  (or `pdf()` etc) and end with dev.off() to make any plot write to a file. You might need to play a bit with the size to get it right. You can then use WinSCP to download your plot file.

```r
png("test.png", width = 500, res=300)

hist(1)

dev.off()
```


## Avoiding that the connection closes.
When you do not respond to the terminal for a while (or close your computer etc), then the server will log you off to save computing resources and avoid a million inactive connections. To avoid this we need a way to keep a session alive so that R is not terminated and keeps working. We also need to be able to connect again to this session (otherwise we would start over again when we connect).

The trick is a program called "screen". It makes a session where the server is both the client and the host. So since the server is always alive the session stays alive. What you you need to do is make such a session *before* you start using R.

* Enter: echo $DISPLAY
* It will show something like: "localhost:10". Make a note of the number.
* Create a new session and give it a name (written in the terminal after connecting to the server): "screen -S name_of_session"
* Type "export DISPLAY=localhost:10" where you replace the number with the number that you found above.
* Now you could type "R" and start working.
* TO manually disconnect from the session (but keeping it alive) type: ctrl+a (you won't see any respose), then type "d". Now you should be thrown back to the server terminal.
* To then reconnect to your session: screen -r name_of_session
* To see which sessions exist if for example you forgot the name: screen -ls

If the plotting starts showing errors with X11 then detach from the session. Type echo $DISPLAY. Attach to the session again and type "export DISPLAY=localhost:10" as above with the number you found. You need to type this in the terminal. Not in R. So you might need to quit R.


## MzMine2
Yes you can now run MZmine2 on the server including the GUI!

* You need an ssh client to connect to the server. A nice client is [mobaxterm](https://mobaxterm.mobatek.net/download.html)
* Open mobaxterm, click "Session" and put the settings similar to this:
![mobaxterm settings](images/moba.png)
* Now login
* You are now in the server terminal
* Type "export DISPLAY=localhost:0" (with no citation marks). This makes the GUI work. You don't need to understand...
* Run the following commands:
  * wget https://github.com/mzmine/mzmine2/releases/download/v2.53/MZmine-2.53-Linux.zip
  * unzip MZmine-2.53-Linux.zip
  * cd MZmine-2.53-Linux/
  * ./startMZmine-Linux
* Now the MzMine2 GUI should start!




## Notes for later...
* https://github.com/jalvesaq/colorout
* can we run parallel code directly on the remote with main thread local?
* should be possible to get plots through [rmote](https://github.com/cloudyr/rmote)

