# Connecting to computerome through ssh

Refer to the [official guide](https://www.computerome.dk/display/C2W/SSH+login+to+Computerome+2.0). If you use MobaXterm pay attention to the settings described.



# Setting up computerome to use R

Open your `.bashrc` with nano or another editor.

```bash
nano ~/.bashrc
```

Add the following line:
`module load  tools gcc intel/perflibs R/4.1.0-GCC-MKL lapack/gcc/64/3.8.0 blas/gcc/64/3.8.0`

Save the file and then run the following:

```bash
source ~/.bashrc
```



## Install Xming

To be able to plot from the server to our own computer we need a small program called xming. It acts like a receiver for the plotting in R.

* Install Xming from (here)[http://sourceforge.net/projects/xming/]
* This program should be running when you use the server.


## Install Git

The program to connect to the server (ssh) that comes with windows is a bit lacking. We will therefore use Git Bash instead.
You need to install Git and set Git Bash as the terminal for RStudio. See how [here](https://github.com/rstudio/rstudio/issues/2224#issuecomment-368260312).



## Compute through RStudio

* Click terminal (next to the console).

* Type 
  ```bash
  export DISPLAY=127.0.0.1:0
  ```
  This makes the plotting work. You don't need to understand...
  
* Type 
  ```bash
  ssh -Y username@ssh.computerome.dk
  ```
  Change to the right computer username. This connects you to the remote server.
  
* Enter your Computerome password and then the one-time password you will receive.

* Type "R" to start R. See the stuff above to make R available and work with plots.

* When you run code, instead of using the shortcut "Ctrl + Enter" which sends the code to the console, use "Ctrl + Alt + Enter" which sends them to the terminal.

* Computerome blocks https connections so to download package you need to set:
```r
options(repos = c(CRAN = "http://cloud.r-project.org"))
```


## Saving plots

The window you get when plotting does not have facilities to save the plot like in RStudio. You need to manually write out a file.

If you are using `ggplot2` you can save a plot like this:

### ggplot2

```r
library(ggplot2)
p <- ggplot(diamonds, aes(x=carat, y = color), color="steelblue") +
      geom_point()

p

ggsave("ggtest.png", p)
```

### Other

For any plot that is not a `ggplot` you can use `png()` (or `pdf()` etc) and end with dev.off() to make any plot write to a file. You might need to play a bit with the size to get it right. You can then use WinSCP to download your plot file.

```r
png("test.png", width = 500, res=300)

hist(1)

dev.off()
```


# Avoiding that the connection closes.

When you do not respond to the terminal for a while (or close your computer etc), then the server will log you off to save computing resources and avoid a million inactive connections. To avoid this we need a way to keep a session alive so that R is not terminated and keeps working. We also need to be able to connect again to this session (otherwise we would start over again when we connect).

The trick is a program called "tmux". It makes a session where the server is both the client and the host. So since the server is always alive the session stays alive. What you you need to do is make such a session *before* you start using R.

* Enter: `echo $DISPLAY`
* It will show something like: `localhost:10`. Make a note of the number.
* Create a new session and give it a name (written in the terminal after connecting to the server): `tmux new -s session_name`
* Type `export DISPLAY=localhost:10` where you replace the number with the number that you found above.
* Now you could type `R` and start working.
* To manually disconnect from the session (but keeping it alive) type: ctrl+b (you won't see any response), then type "d". Now you should be thrown back to the server terminal.
* To then reconnect to your session: `tmux attach-session -t session_name`
* To see which sessions exist if for example you forgot the name: `tmux -ls`

If the plotting starts showing errors with X11 then detach from the session. Type `echo $DISPLAY`. Attach to the session again and type `export DISPLAY=localhost:10` as above with the number you found. You need to type this in the terminal. Not in R. So you might need to quit R.



# Transferring files from i-drive to computerome

## Using GUI from your computer
Use winSCP.

You could also use winSCP from our own calc computer so you have a wired connection you can leave on.



## From KU computer by commandline

KU-IT has confirmed that there is no way to access the i-drive from computerome.
I have found a workaround though. Instead of accessing the i-drive from computerome we can connect to computerome from a computer with access to the idrive. To make it fast we need a computer with a wired connection. For now we have the KU-IT compute12 server that can be used. We could potentially use a lab computer too (there you could also use a visual sftp client and avoid dealing with code).

Here is how to do it:

First you go to the folder you wish to copy *from* (e.g. on the compute12 server).
This can be a folder that contains a folder where you want to copy the entire content.

```bash
cd /home/tmh331/groupdirs/SCIENCE-NEXS-Metabolomics
```

Then you start an sftp connect to computerome:

```bash
sftp [username]@ssh.computerome.dk
```

Then browse to the target folder on computerome:

```bash
cd /home/projects/ku_00093/data
```

Now you can start the copy:

```bash
put -r mzML
```

So in effect you transfer the `mzML` (`/home/tmh331/groupdirs/SCIENCE-NEXS-Metabolomics/mzML`) folder and all content (recursive, `-r`) to `/home/projects/ku_00093/data` on computerome.



# Using RStudio

First you connect to computerome by ssh. Refer to the [official guide](https://www.computerome.dk/display/C2W/SSH+login+to+Computerome+2.0).

Then you make a session that will not close if you should close your connection to the server:
```bash
tmux new -s rstudio_spaceomics
```
replace "rstudio_spaceomics" with any name you want.

Now you request a node you you are allowed to run big jobs on:
```bash
qsub -I -X -W group_list=ku_00093 -A ku_00093 -l nodes=1:fatnode:ppn=20,mem=200g,walltime=99:00:00
```
Here we ask for a node for the usergroup "ku_00093", project "ku_00093", 20 cores, 200g of memory and we ask to keep the job running for 99 hours (~4 days).

We load some modules we need and start Rstudio server:

```bash
module load tools gcc git-lfs/3.3.0 intel/perflibs R/4.3.0 rstudio-server/2022.07.2-576
rstudio2022.07.2-576-R.4.2.0-install
```

Make a note of your session name. The command prompt will say something like: `[username@g-02-c0100 ~]$`. It is the part after "@" you need.
You can now close your ssh connection.

From a command prompt on *your local computer* you then type:

```bash
ssh -L 8787:<your interactive node name>:8787 <your username>@ssh.computerome.dk
```
For example `ssh -L 8787:g-02-c0100:8787 janstan@ssh.computerome.dk`
This will forward the Rstudio port from the server to your computer and you can now access Rstudio at: http://localhost:8787/

If you close your computer (or the command prompt) you just need to run this last line again to be able to re-connect to Rstudio. That is given that the node you requested is still alive.




# Notes for later...

* https://github.com/jalvesaq/colorout
* can we run parallel code directly on the remote with main thread local?
* should be possible to get plots through [rmote](https://github.com/cloudyr/rmote)
